package com.test.factory.mapper;

import com.test.factory.domain.Student;

import java.util.List;

public interface IStudentMapper {

    public List<Student> selectStudent ();
}
