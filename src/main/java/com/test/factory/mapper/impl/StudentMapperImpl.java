package com.test.factory.mapper.impl;

import com.test.factory.domain.Student;
import com.test.factory.mapper.IStudentMapper;

import java.util.ArrayList;
import java.util.List;

public class StudentMapperImpl implements IStudentMapper {
    public List<Student> selectStudent () {
        Student student = new Student();
        student.setId(5);
        student.setName("张三");
        student.setChinese(56.89);
        student.setEnglish(47.8);
        student.setMath(98.5);
        List<Student> studentList = new ArrayList<Student>();
        studentList.add(student);

        return studentList;
    }
}
