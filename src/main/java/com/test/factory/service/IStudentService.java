package com.test.factory.service;

import com.test.factory.domain.Student;

import java.util.List;

public interface IStudentService {

    public List<Student> selectStudent ();

    List<Student> selectStudentOfFactory ();
}
