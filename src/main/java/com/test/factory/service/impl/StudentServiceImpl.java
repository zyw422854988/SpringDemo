package com.test.factory.service.impl;

import com.test.factory.beanFactory.BeanFactory;
import com.test.factory.domain.Student;
import com.test.factory.mapper.IStudentMapper;
import com.test.factory.mapper.impl.StudentMapperImpl;
import com.test.factory.service.IStudentService;

import java.util.List;

public class StudentServiceImpl implements IStudentService {

    public List<Student> selectStudent () {
        IStudentMapper studentMapper = new StudentMapperImpl();
        return studentMapper.selectStudent();
    }

    public List<Student> selectStudentOfFactory () {
        // IStudentMapper studentMapper = (IStudentMapper) BeanFactory.getBeanOfNew("studentMapper");
        IStudentMapper studentMapper = (IStudentMapper) BeanFactory.getBean("studentMapper");
        return studentMapper.selectStudent();
    }
}
