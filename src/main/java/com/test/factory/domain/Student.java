package com.test.factory.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Student {
    private Integer id;
    private String name;
    private Double chinese;
    private Double english;
    private Double math;
}
