package com.test.factory.beanFactory;

import com.test.factory.service.impl.StudentServiceImpl;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class BeanFactory {
    private static Map<String,String> maps;
    private static Map<String,Object> mapBeans;
    static {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("beanFactory");
        Enumeration<String> keys = resourceBundle.getKeys();
        maps = new HashMap<String, String>();
        mapBeans = new HashMap<String, Object>();
        while (keys.hasMoreElements()){
            String key = keys.nextElement();
            String value = resourceBundle.getString(key);
            System.out.println(key +"----" + value);
            try {
                Object object = Class.forName(value).newInstance();
                mapBeans.put(key,object);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        // maps = new HashMap<String, String>();
        // maps.put("studentService","com.test.factory.service.impl.StudentServiceImpl");
        // maps.put("studentMapper","com.test.factory.mapper.impl.StudentMapperImpl");
    }

    public static Object getSingleBean(String beanName){
        return mapBeans.get(beanName);
    }

    public static Object getBean(String beanName){

        try {
            Object o = Class.forName(maps.get(beanName)).newInstance();
            return o;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
