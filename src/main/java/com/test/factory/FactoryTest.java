package com.test.factory;

import com.test.factory.beanFactory.BeanFactory;
import com.test.factory.domain.Student;
import com.test.factory.service.IStudentService;
import com.test.factory.service.impl.StudentServiceImpl;
import org.junit.Test;

import java.util.List;

public class FactoryTest {

    @Test
    public void testNormal(){

        IStudentService studentService = new StudentServiceImpl();
        List<Student> studentList = studentService.selectStudent();
        for (Student student : studentList) {
            System.out.println(student);
        }
    }

    @Test
    public void testFactory(){
        // IStudentService studentService = (IStudentService) BeanFactory.getBeanOfNew("studentService");
        // IStudentService studentService = (IStudentService) BeanFactory.getBean("studentService");

        for (int i = 0; i<5; i++){
            IStudentService studentService = (IStudentService) BeanFactory.getSingleBean("studentService");
            System.out.println(studentService);
        }
        /*List<Student> studentList = studentService.selectStudentOfFactory();
        for (Student student : studentList) {
            System.out.println(student);
        }*/
    }
}
