package com.test.utils;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;


public class Java8DateUtil {

    public static final String DATE_FORMAT_FULL = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_FORMAT_0 = "yyyy-MM-dd 00:00:00";

    public static final String DATE_FORMAT_24 = "yyyy-MM-dd 23:59:59";

    public static final String DATE_FORMAT_SHORT = "yyyy-MM-dd";

    public static final String DATE_FORMAT_COMPACT = "yyyyMMdd";

    public static final String DATE_FORMAT_COMPACTFULL = "yyyyMMddHHmmss";

    public static final String DATE_FORMAT_FULL_MSEL = "yyyyMMddHHmmssSSSS";

    public static final String DATE_YEAR_MONTH = "yyyyMM";

    public static final String DATE_FORMAT_FULL_MSE = "yyyyMMddHHmmssSSS";

    public static final ZoneId CHINA_ZONE = ZoneId.systemDefault();


    /**
     * 获取系统当前日期
     *
     * @return
     */
    public static Date getCurrentDate() {
        return new Date();
    }

    /**
     * 获取系统当前日期
     *
     * @return
     */
    public static LocalDateTime getCurrentLocalDateTime() {
        return LocalDateTime.now();
    }

    /**
     * 根据时间格式返回对应的String类型的时间
     *
     * @param format
     * @return
     */
    public static String getCurDateTime(String format) {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        return now.format(dateTimeFormatter);
    }

    /**
     * 得到当前日期
     *
     */
    public static String getCurDateTimeFull() {
        return getCurDateTime(DATE_FORMAT_FULL);
    }


    /**
     * 得到当前日期
     *
     */
    public static String getCurDateTime1() {
        return getCurDateTime(DATE_FORMAT_FULL);
    }

    /**
     * 得到当前日期YYYYMM格式
     *
     */
    public static String getCurDateYYYYMM() {
        return getCurDateTime(DATE_YEAR_MONTH);
    }


    /**
     * 得到当前日期
     *
     */
    public static String getCurDateYYYYMMDD() {
        return getCurDateTime(DATE_FORMAT_COMPACT);
    }

    /**
     * 判断是否是今天
     *
     * @param strDate
     * @return
     */
    public static boolean isCurrentDay(String strDate) {
        LocalDate strLocalDate = LocalDate.parse(strDate);
        if (LocalDate.now().getYear() == strLocalDate.getYear()) {
            MonthDay monthDay = MonthDay.from(strLocalDate);
            MonthDay today = MonthDay.from(LocalDate.now());
            return monthDay.equals(today);
        }
        return false;
    }


    /**
     * 当前日期时间戳(yyyyMMddHHmmssSSSS)
     *
     * @return
     */
    public static String getTimeStamp() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT_FULL_MSEL);
        return now.format(dateTimeFormatter);
    }

    /**
     * 日期转字符串
     *
     * @return String
     */
    public static String parseDateToString(Date thedate, String format) {
        if (thedate != null) {
            Instant instant = thedate.toInstant();
            ZoneId zone = ZoneId.systemDefault();
            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
            return localDateTime.format(dateTimeFormatter);
        }
        return null;
    }


    /**
     * 获取指定日期0点时间
     * @param thedate
     * @return
     */
    public static String parseDateToStringDay0(Date thedate) {
        if (thedate != null) {
            Instant instant = thedate.toInstant();
            ZoneId zone = ZoneId.systemDefault();
            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT_0);
            return localDateTime.format(dateTimeFormatter);
        }
        return null;
    }


    /**
     * 获取指定日期24点时间
     * @param thedate
     * @return
     */
    public static String parseDateToStringDay24(Date thedate) {
        if (thedate != null) {
            Instant instant = thedate.toInstant();
            ZoneId zone = ZoneId.systemDefault();
            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT_24);
            return localDateTime.format(dateTimeFormatter);
        }
        return null;
    }

    /**
     * 日期转字符串 "yyyy-MM-dd HH:mm:ss"
     *
     * @param thedate
     * @return
     */
    public static String parseDateToString(Date thedate) {
        return parseDateToString(thedate, DATE_FORMAT_FULL);
    }

    /**
     * 字符串转日期
     *
     * @return Date
     * @author
     */
    public static Date parseStringToDate(String thedate, String format) {
        DateFormat sdf = new SimpleDateFormat(format);
        Date dd1 = null;
        try {
            dd1 = sdf.parse(thedate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dd1;
    }


    /**
     * 得到当前日期的前N天时间
     *
     * @param format
     * @param day
     * @return
     */
    public static String beforeNDaysDate(String format, int day) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        if (day > 0) {
            return LocalDateTime.now().minusDays(day).format(dateTimeFormatter);
        }
        return null;
    }

    /**
     * 获得N个月后的日期
     * @param theDate
     * @param month
     * @param format
     * @return
     */
    public static String afterNMonthDate(String theDate, int month, String format) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        return LocalDateTime.parse(theDate, dateTimeFormatter).plusMonths(month).format(dateTimeFormatter);

    }

    /**
     * 得到N天后的日期
     *
     * @param theDate 某日期格式 yyyy-MM-dd
     * @param nDayNum N天
     * @return String N天后的日期
     */
    public static String afterNDaysDate(String theDate, Integer nDayNum, String format) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        return LocalDateTime.parse(theDate, dateTimeFormatter).plusDays(nDayNum).format(dateTimeFormatter);
    }

    /**
     * 得到N小时后的日期
     *
     * @param theDate  时间
     * @param nHourNum N小时数
     * @param format   时间格式
     * @return
     */
    public static String afterNHoursDate(String theDate, Integer nHourNum, String format) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        return LocalDateTime.parse(theDate, dateTimeFormatter).plusHours(nHourNum).format(dateTimeFormatter);
    }

    /**
     * 得到N分钟后的日期
     *
     * @param theDate
     * @param nMinNum
     * @param format
     * @return
     */
    public static String afterNMinsDate(String theDate, Integer nMinNum, String format) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        return LocalDateTime.parse(theDate, dateTimeFormatter).plusMinutes(nMinNum).format(dateTimeFormatter);
    }

    /**
     * 得到N秒后的日期
     *
     * @param theDate
     * @param nSecNum
     * @param format
     * @return
     */
    public static String afterNSecondsDate(String theDate, Integer nSecNum, String format) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        return LocalDateTime.parse(theDate, dateTimeFormatter).plusSeconds(nSecNum).format(dateTimeFormatter);
    }

    /**
     * 比较两个字符串格式日期大小,带格式的日期
     *
     * @param strdat1 日期1
     * @param strdat2 日期2
     * @param format  格式
     * @return 小于返回true 大于返回false
     */
    //
    public static boolean isBefore(String strdat1, String strdat2, String format) {
        try {
            Date dat1 = parseStringToDate(strdat1, format);
            Date dat2 = parseStringToDate(strdat2, format);
            return dat1.before(dat2);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    /**
     * 得到上一个月或者下一个月的日期
     * @param theDate
     * @param month
     * @param formatStr
     * @return
     */
    public static String getDayafterMonth(String theDate, int month, String formatStr) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(formatStr);
        return LocalDateTime.parse(theDate).plusMonths(month).format(dateTimeFormatter);

    }

    /**
     * 将秒转换为小时分秒等
     *
     * @param sec
     * @return
     */
    public static  String changeTime(int sec) {
        String temp = "";
        if (sec < 60) {
            temp = "" + sec + "秒";
        } else if (sec < 3600) {
            temp = "" + sec / 60 + "分" + sec % 60 + "秒";
        } else {
            temp = "" + sec / 3600 + "小时" + (sec % 3600) / 60 + "分" + sec % 60 + "秒";
        }
        return temp;
    }

    /**
     *
     * 计算两个日期相差天数
     *
     * @param end   结束日期
     * @param start 开始日期
     */
    public static int getSubDays(String end, String start) {
        LocalDate startDate = LocalDate.parse(start.substring(0,10));
        LocalDate endDate = LocalDate.parse(end.substring(0,10));
        Long between = ChronoUnit.DAYS.between(startDate, endDate);
        return between.intValue();
    }


    /**
     *
     * @param time1
     * @param time2
     * @return
     * @throws Exception
     */
    public static String getTimeDiff(Date time1, Date time2) throws Exception {
        long l = time1.getTime() - time2.getTime();
        String returnStr = "";
        long day = l / (24 * 60 * 60 * 1000);
        if (day > 0) {
            returnStr += (day + "天");
        }
        long hour = (l / (60 * 60 * 1000) - day * 24);
        if (hour > 0) {
            returnStr += (hour + "小时");
        }
        long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
        if (min > 0) {
            returnStr += (min + "分");
        }
        long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        if (s > 0) {
            returnStr += (s + "秒");
        }
        return returnStr;
    }


    /**
     * 日期时间转字符串函数
     * 返回ISO标准的日期字符串
     *
     * @param localDateTime 日期时间对象
     * @return 日期字符串
     */
    public static String dateTime2Str(LocalDateTime localDateTime) {
        return localDateTime.format(DateTimeFormatter.ISO_DATE_TIME);
    }

    /**
     * 计算两个日期之间相差的天数
     *
     * @param date1 起始日期
     * @param date2 结束日期
     * @return
     */
    public static int daysBetween(LocalDate date1, LocalDate date2) {
        Period period = Period.between(date1, date2);
        return period.getDays();
    }

    /**
     * 计算两个日期之间相差的月数
     *
     * @param date1 起始日期
     * @param date2 结束日期
     * @return
     */
    public static int monthsBetween(LocalDate date1, LocalDate date2) {
        Period period = Period.between(date1, date2);
        return period.getMonths();
    }

    /**
     * 计算两个日期之间相差的年数
     *
     * @param date1 起始日期
     * @param date2 结束日期
     * @return
     */
    public static int yearsBetween(LocalDate date1, LocalDate date2) {
        Period period = Period.between(date1, date2);
        return period.getYears();
    }

    /**
     * 计算两个日期之间相差的天数
     *
     * @param date1 起始日期
     * @param date2 结束日期
     * @return date2 <date1 <0
     */
    public static int daysBetween(Date date2, Date date1) {
        Instant instantDate1 = date1.toInstant();
        Instant instantDate2 = date2.toInstant();
        LocalDate localDate1 = instantDate1.atZone(CHINA_ZONE).toLocalDate();
        LocalDate localDate2 = instantDate2.atZone(CHINA_ZONE).toLocalDate();
        instantDate1.atZone(CHINA_ZONE);
        Period period = Period.between(localDate1, localDate2);
        return period.getDays();
    }



    /**
     * 比较两个日期大小
     */
    public static int compare_date(String DATE1, String DATE2) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dt1 = df.parse(DATE1);
            Date dt2 = df.parse(DATE2);
            if (dt1.getTime() > dt2.getTime()) {
                return 1;
            } else if (dt1.getTime() < dt2.getTime()) {
                return -1;
            } else {
                return 0;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return 0;
    }

    /**
     * 计算两个日期之间相差的月数
     *
     * @param date1 起始日期
     * @param date2 结束日期
     * @return
     */
    public static int monthsBetween(Date date1, Date date2) {
        Instant instantDate1 = date1.toInstant();
        Instant instantDate2 = date2.toInstant();
        LocalDate localDate1 = instantDate1.atZone(CHINA_ZONE).toLocalDate();
        LocalDate localDate2 = instantDate2.atZone(CHINA_ZONE).toLocalDate();
        instantDate1.atZone(CHINA_ZONE);
        Period period = Period.between(localDate1, localDate2);
        return period.getMonths();
    }

    /**
     * 计算两个日期之间相差的年数
     *
     * @param date1 起始日期
     * @param date2 结束日期
     * @return
     */
    public static int yearsBetween(Date date1, Date date2) {
        Instant instantDate1 = date1.toInstant();
        Instant instantDate2 = date2.toInstant();
        LocalDate localDate1 = instantDate1.atZone(CHINA_ZONE).toLocalDate();
        LocalDate localDate2 = instantDate2.atZone(CHINA_ZONE).toLocalDate();
        instantDate1.atZone(CHINA_ZONE);
        Period period = Period.between(localDate1, localDate2);
        return period.getYears();
    }

    /**
     * 获取指定日期对象当前月的起始日
     *
     * @param localDate 指定日期
     * @return
     */
    public static int getFirstDayInMonth(LocalDate localDate) {
        LocalDate result = localDate.with(TemporalAdjusters.firstDayOfMonth());
        return result.getDayOfMonth();

    }

    /**
     * 获取指定日期对象的当前月的结束日
     *
     * @param localDate 指定日期
     * @return
     */
    public static int getLastDayInMonth(LocalDate localDate) {
        LocalDate result = localDate.with(TemporalAdjusters.lastDayOfMonth());
        return result.getDayOfMonth();
    }


    /**
     * 获取指定日期对象本月的某周某天的日期
     *
     * @param localDate  日期对象
     * @param weekNumber 周
     * @param dayNumber  日
     * @return
     */
    public static LocalDate getLocalDateBydayAndWeek(LocalDate localDate, int weekNumber, int dayNumber) {
        return localDate.with(TemporalAdjusters.dayOfWeekInMonth(weekNumber, DayOfWeek.of(dayNumber)));
    }


    /**
     * 将时间戳转换成日期格式
     * @param timestamp
     * @return
     */
    public static Date getDateTimeOfTimestamp(long timestamp) {
        Instant instant = Instant.ofEpochMilli(timestamp);
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        ZonedDateTime zdt = localDateTime.atZone(zone);
        return Date.from(zdt.toInstant());
    }

    /**
     * 按天数增加日期
     */
    public static LocalDate addLocalDateByDay(Date date , Integer days) {
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDate localDate = instant.atZone(zone).toLocalDate();
        return localDate.plusDays(days);
//        return dateTime.plusDays(days).toDate();
    }

    public static String localDateToString(LocalDate localDate){
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern(DATE_FORMAT_SHORT);
        return localDate.format(fmt);
    }


    /**
     * 参数为字符串增加天数
     * @return
     *       targetDate 需为  yyyy-MM-dd 或 格式
     */
    public static String addDaysByStr(String targetDate, int days){
        DateTimeFormatter f = DateTimeFormatter.ofPattern(DATE_FORMAT_SHORT);
        //字符串转为时间
        LocalDate strLocalDate = LocalDate.parse(targetDate.substring(0,10));
        return strLocalDate.plusDays(days).format(f);
    }

    public static String addMonthsByStr(String targetDate,int month){
        DateTimeFormatter f = DateTimeFormatter.ofPattern(DATE_FORMAT_SHORT);
        //字符串转为时间
        LocalDate strLocalDate = LocalDate.parse(targetDate.substring(0,10));
        return strLocalDate.plusMonths(month).format(f);
    }

    /**
     * 
     * @Description :  获取某天的开始时间
     * @Param : targetDate 需为  yyyy-MM-dd 格式
     */
    public static String getStartTimeOfDay(String targetDate){
        DateTimeFormatter f = DateTimeFormatter.ofPattern(DATE_FORMAT_FULL);
        //字符串转为日期
        LocalDate strLocalDate = LocalDate.parse(targetDate.substring(0,10));
        //设置零点
        LocalDateTime beginTime = LocalDateTime.of(strLocalDate,LocalTime.MIN);
        return beginTime.format(f);
    }

    /**
     * 
     * @Description :  获取某天的结束时间
     * @Param : targetDate 需为  yyyy-MM-dd 格式
     */
    public static String getEndTimeOfDay(String targetDate){
        DateTimeFormatter f = DateTimeFormatter.ofPattern(DATE_FORMAT_FULL);
        //字符串转为日期
        LocalDate strLocalDate = LocalDate.parse(targetDate.substring(0,10));
        //设置结束时间
        LocalDateTime beginTime = LocalDateTime.of(strLocalDate,LocalTime.MAX);
        return beginTime.format(f);
    }

    /**
     * 获取某月第一天
     */
    public static String getStartDayOfMonth(String date) {
        DateTimeFormatter f = DateTimeFormatter.ofPattern(DATE_FORMAT_FULL);
        LocalDate strLocalDate = LocalDate.parse(date.substring(0,7)+"-01");
        LocalDate startDayOfMonth = strLocalDate.with(TemporalAdjusters.firstDayOfMonth());
        //设置零点
        LocalDateTime beginTime = LocalDateTime.of(startDayOfMonth,LocalTime.MIN);
        return beginTime.format(f);
    }
    /**
     * 获取某月最后一天
     */
    public static String getEndDayOfMonth(String date) {
        DateTimeFormatter f = DateTimeFormatter.ofPattern(DATE_FORMAT_FULL);
        LocalDate strLocalDate = LocalDate.parse(date.substring(0,7)+"-01");
        LocalDate endDayOfMonth = strLocalDate.with(TemporalAdjusters.lastDayOfMonth());
        //设置结束时间
        LocalDateTime endTime = LocalDateTime.of(endDayOfMonth,LocalTime.MAX);
        return endTime.format(f);
    }
}
