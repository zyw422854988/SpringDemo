package com.test.instance;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestStaticFactory {

    @Test
    public void testStaticFactory(){
        // Emp empBean = StaticFactory.getEmpBean();
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-beans.xml");
        Emp emp = context.getBean("emp", Emp.class);
        emp.print();
    }

    @Test
    public void testFactory(){
        // BeanFactory beanFactory = new BeanFactory();
        // Emp empBean = beanFactory.getEmpBean();
        // Emp empBean = StaticFactory.getEmpBean();
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-beans.xml");
        Emp emp = context.getBean("emp02", Emp.class);
        emp.print();
    }
}
