package com.test.instance;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Emp {
    private Integer id;
    private String name;

    public void print(){
        System.out.println("员工信息");
    }
}
