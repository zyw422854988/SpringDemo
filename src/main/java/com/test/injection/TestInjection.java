package com.test.injection;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestInjection {

    @Test
    public void testInjection(){
        // 读取配置文件，并获取容器
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-beans.xml");
        InjectionDemo injectionDemo = context.getBean("injectionDemo", InjectionDemo.class);
        System.out.println(injectionDemo);
    }
}
