package com.test.injection;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@ToString
@Setter
@Getter
public class InjectionDemo {

    private Integer id;
    private String name;
    private Demo demo;
    private Date date;



}
