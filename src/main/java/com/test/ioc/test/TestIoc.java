package com.test.ioc.test;

import com.test.ioc.domain.Account;
import com.test.ioc.service.IAccountService;
import com.test.ioc.service.impl.AccountServiceImpl;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class TestIoc {

    @Test
    public void testSelectAll(){
        IAccountService accountService = new AccountServiceImpl();
        Account account = accountService.selectAllAccount();
        System.out.println(account);
    }

    @Test
    public void testIocSelectAll(){
        // 读取配置文件，获取容器
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-beans.xml");
        IAccountService accountService = context.getBean("accountService",IAccountService.class);
        Account account = accountService.selectAllAccount();
        System.out.println(account);
    }

    @Test
    public void testAccount(){
        // 读取配置文件，获取容器
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-beans.xml");
        Account account = context.getBean("account", Account.class);
        System.out.println(account.getString());
    }

    @Test
    public void testFileSystem(){
        // 读取配置文件，获取容器
        ApplicationContext context = new FileSystemXmlApplicationContext("F:\\beans\\spring-beans.xml");
        Account account = context.getBean("account", Account.class);
        System.out.println(account.getString());
    }

    @Test
    public void testScope(){
        // 读取配置文件，获取容器
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-beans.xml");
        for (int i=0; i< 5; i++){
            Account account = context.getBean("account", Account.class);
            System.out.println(account);
        }

        try {
            Thread.sleep(5000);
            ((ClassPathXmlApplicationContext) context).destroy();
            ((ClassPathXmlApplicationContext) context).close();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
