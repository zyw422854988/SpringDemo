package com.test.ioc.service;

import com.test.ioc.domain.Account;

public interface IAccountService {
    public Account selectAllAccount();
}
