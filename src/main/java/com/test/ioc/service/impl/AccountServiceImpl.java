package com.test.ioc.service.impl;

import com.test.ioc.domain.Account;
import com.test.ioc.service.IAccountService;

public class AccountServiceImpl implements IAccountService {
    public Account selectAllAccount () {
        Account account = new Account();
        account.setId(10);
        account.setName("张三");
        return account;
    }
}
