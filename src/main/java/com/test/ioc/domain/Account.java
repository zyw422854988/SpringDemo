package com.test.ioc.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
// @ToString
public class Account {
    private Integer id;
    private String name;

    public Account () {
    }

    public void initMethod(){
        // System.out.println("执行了初始化方法");
    }

    public void destroyMethod(){
        // System.out.println("执行了销毁方法");
    }

    public String getString(){
        return "aaaa";
    }
}
